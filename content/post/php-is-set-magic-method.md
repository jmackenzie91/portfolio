---
title: PHP Is Set Magic Method
author: John Mackenzie
date: 2018-02-19
post_date: 19th February 2018
excerpt: The _isset() magic method. what is it? and when do we use it?
description: The _isset() magic method. what is it? and when do we use it?
image: /images/magicians-hat.jpg
---

## What it does
The isset magic method is called when an unset or inaccessible  property is called on an object.

```php
class user {

	public function __construct($name, $age)
	{
    //
	}

	public function __isset($value)
	{
		return 'That property does not exist';
	}
}

$user = new User();
$user->age; // returns 'That property does not exist'
```

As you can see abce we are trying to access the age property on the class, but unfortunately there isn't one. This would call the isset() magic method and we can deal with it how we want to.

```php
class user {

	protected $properties[];

	public function __construct($name, $age)
	{
		$this->properties['name'] = $name;
		$this->properties['age'] = $age;
	}

	public function __isset($value)
	{
    if (isset($this->properties[$value])) {
      return $this->properties[$value];
    }
		return 'That property does not exist';
	}
}

$user = new User('John', 26);
$user->age; // returns 26
```
Above you can see that we look for the property in the properties property.

This is how laravels eloquent works with its model properties, definitely worth a look [here](https://laravel.com/docs/5.5/eloquent-mutators)
