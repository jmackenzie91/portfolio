---
title: "Local Development With Golang, Dynamodb, and Docker"
date: 2019-09-26T07:44:07+01:00
draft: true

post_date: 26th September 2019
excerpt: I have recently been tasked with creating a Golang Microservice that uses AWS's Dynamodb as its data store. This is the writeup.
description: I have recently been tasked with creating a Golang Microservice that uses AWS's Dynamodb as its data store. This is the writeup.
image: /images/golang-dynamodb.png
---

The finished project will be a go binary, that will live in an AWS EC2 instance and speak to Dynamodb.
Locally I will have two docker images a [scratch](https://hub.docker.com/_/scratch) image that will contain my application code.
As well as a [amazon/dynamodb-local](https://hub.docker.com/r/amazon/dynamodb-local) container that the code in scratch will call into.

## Let's get started

To avoid a wall of text I have split this article up into two main parts

1. [Setting up the database environment](/post/setting-up-dynamodb-for-local-development/)
2. Setting up the codebase
3. The finished result. Testing with Postman

### Setting up the codebase

