---
title: "Unit Testing Api Clients in Golang"
author: John Mackenzie
date: 2019-02-25T09:46:50Z
post_date: 2019-02-25T09:46:50Z
draft: true

excerpt: please_enter_an_excerpt
description: please_enter_an_excerpt
image: /images/image_name.jpg
---

In this blog post I will be writing unit tests for a client of a fake api. The api will have one endpoint `/artists`.
I will demonstrate to you the process that I use to ensure my test coverage is a strong as possible.

## My workflow

### 1. Find out all the possible responses that `/artists` route can return.

Hopefully the api has good documentation, [swagger](https://swagger.io/) docs or a [RAML](https://raml.org/) spec for example.
If not you could always test it yourself by sending invalid requests to unknown resources/entities ect.

### 2. Save the returned body of each request into a file under proj_root/testdata for each of these responses.
Go automatically ignores the `testdata` dir if present.

The content of these files will be used in the body of my mocked responses.

### 3. Add an additional two client network errors, unable to connect to server and response timeout.

To fully test an api client it is important to test for in the scenario that we will not event be able to connect
to the server for whatever reason.

### 4. Mock out the server

In a nutshell we will be creating a fake server (in go code), we will set the response, and our client will interact with it.
By doing this we will be able to test how our client will react to our scenarios.

### 5. Write test cases for each of the possible responses

I follow TDD best practices and write one test at a time, fixing and breaking tests as i do so. However for this blog post I will display all of them together.

## The API

We have a simple fictional api, imaginetively called `music_api`. This API currently only has one endpoint /artists, which returns all the artists in the db.

Below are all the possible responses;

 1. Successful 200

```
{
  "links": {},
  "data": [
    {
      "uuid": "216a0a95-85fc-42e9-ae65-c16fdef41b41",
      "name": "Arctic Monkeys"
    },
    {
      "uuid": "46881c16-609d-4b62-bc16-c92f6a7abed0",
      "name": "Arcade Fire"
    }
  ]
}
```

2. Successful 200 but not results found

```
{
  "links": {},
  "data": []
}
```

3. Internal Server Error 500

4. Request Timed Out 403

5. Unauthorised 401


## The Test Cases

```
testCases := []struct {
	Name      string // This will be displayed if a test errors, showing the specific failing test case
	InputBody string
	// Input
	InputStatusCode int
	InputTimeout    time.Duration
	// end input
	ExpectedResponseData *ArtistsIndexResponse
	ExpectedError        error
}{
	{
		Name:            "GET artists/ returns no results",
		InputStatusCode: http.StatusOK,
		InputBody:       "../../test_data/clients/music/artists/index/no-results.json",
		ExpectedResponseData: &ArtistsIndexResponse{
			Data: []Artist{},
		},
	},
	{
		Name:            "GET artists/ returns 2 results",
		InputStatusCode: http.StatusOK,
		InputBody:       "../../test_data/clients/music/artists/index/three-results.json",
		ExpectedResponseData: &ArtistsIndexResponse{
			Data: []Artist{
				{
					UUID: "216a0a95-85fc-42e9-ae65-c16fdef41b41",
					Name: "Arctic Monkeys",
				},
				{
					UUID: "46881c16-609d-4b62-bc16-c92f6a7abed0",
					Name: "Arcade Fire",
				},
			},
		},
	},
	{
		Name:            "GET artists/ returns gracefully when timeout",
		InputStatusCode: http.StatusRequestTimeout,
		InputBody:       "../../test_data/clients/music/artists/index/no-results.json",
		InputTimeout:    time.Second * 3,
		ExpectedError:   TimeoutError,
	},
	{
		Name:            "GET artists/ returns gracefully when 500 error",
		InputStatusCode: http.StatusInternalServerError,
		InputBody:       "../../test_data/clients/music/artists/index/no-results.json",
		ExpectedError:   InternalServerError,
	},
	{
		Name:            "GET artists/ returns gracefully when 401 unauthoriser error",
		InputStatusCode: http.StatusUnauthorized,
		InputBody:       "../../test_data/clients/music/artists/index/no-results.json",
		ExpectedError:   UnauthorisedError,
	},
}
```

## The Unit test function

We will now loop through each of the test cases

```
func TestArtistIndex(t *testing.T) {

// let us loop through each of the unit tests
	for _, tc := range testCases {
		// Arrange
		// here we are telling the server how to return, configured in the unit test cases above
		// as you can see the InputStatusCode, InputTimeout and InputBody
		h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.InputStatusCode)
			time.Sleep(tc.InputTimeout)
			f, _ := ioutil.ReadFile(tc.InputBody)
			w.Write(f)
		})

		// instanciate the fake server
		s := httptest.NewServer(h)
		defer s.Close()

		// create a new client that dials into our mock server
		mockCli := &http.Client{
			Transport: &http.Transport{
				DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
					return net.Dial(network, s.Listener.Addr().String())
				},
			},
			Timeout: time.Second * 3,
		}

		cli := New()
		cli.Client = mockCli

		// Act
		res, err := cli.GetArtists()

		// Assert
		if !reflect.DeepEqual(tc.ExpectedResponseData, res) {
			t.Errorf("%s\nUnexpected Response\nexpected:\n%+v\noutput:\n%+v", tc.Name, tc.ExpectedResponseData, res)
		}

		if !reflect.DeepEqual(tc.ExpectedError, err) {
			t.Errorf("%s\nUnexpected Error\nexpected:\n%+v\noutput:\n%+v", tc.Name, tc.ExpectedError, err)
		}
	}
}
```

## The Client Code

To finish things off below is the actual client code that we have been testing.

```
package music

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"time"
)

type Artist struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type ArtistsIndexResponse struct {
	Links struct{}
	Data  []Artist
}

func New() Client {
	return Client{
		Client: &http.Client{
			Timeout: time.Second * 5,
		},
		BaseUrl: "http://example-music-api.com",
	}
}

type Client struct {
	Client  *http.Client
	BaseUrl string
}

func (c Client) GetArtists() (*ArtistsIndexResponse, error) {

	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/artists", c.BaseUrl), nil)
	res, err := c.Client.Do(req)

	// Handle network errors
	if err != nil {
		return nil, handleNetworkError(err)
	}
	defer res.Body.Close()

	resBody, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return nil, UnableToParseResponseBOdy
	}

	// Handle error response codes
	switch res.StatusCode {
	case http.StatusInternalServerError:
		return nil, InternalServerError
	case http.StatusUnauthorized:
		return nil, UnauthorisedError
	}

	resStruct := ArtistsIndexResponse{}
	err = json.Unmarshal(resBody, &resStruct)

	if err != nil {
		return nil, JsonError
	}

	return &resStruct, nil
}

func handleNetworkError(err error) error {
	switch err := err.(type) {
	case net.Error:
		if err.Timeout() {
			return TimeoutError
		}
	case *url.Error:
		if err, ok := err.Err.(net.Error); ok && err.Timeout() {
			return TimeoutError
		}
	}
	return err
}

var InternalServerError = errors.New("Internal Server error returned from Music API")
var TimeoutError = errors.New("Timeout exceeded")
var JsonError = errors.New("Failed to parse json response")
var UnauthorisedError = errors.New("Unauthorised")
var UnableToParseResponseBOdy = errors.New("Unable to parse response body")
```