---
title: My Must Have Chrome Extensions
author: John Mackenzie
date: 2017-04-19
post_date: 6th January 2018
excerpt: The chrome plugins that i simply can't live without.
description: The chrome plugins that i simply can't live without.
image: /images/plug.jpg
---

Chrome is my browser of choice, its development tools are far ahead of any other browser that i have come across, (I am willing to have my mind changed). The stack that I work with most in Vue, Laravel, Nginx, and these are my most used ad beloved chrome plugins.

As a sidenote;

Plugins are good and greatly increase browser functionality and can save you a shed load of time. Be aware however that for every external piece of code you plug into your browser is another possible security vulnerability. I suggest that you only install plugins that are actually useful to you (not just for the hell of it) and check the reviews on the chrome dev store, alongside looking up the author to make sure there are no shady characters involved. Other than that lets go…

### Ublock

First up is Ublock, a must have. Not just for development but for the overall browsing experience. It iblocks any ads and tracking scripts that you may come across in your browsing journey. This greatly speeds up your online experience and limits the amount of data the likes of Google, and Facebook can gather from you. I fully understand how ad revenue is used as the lifeblood to keep our favourite online services up and running but some sites go too far, cramming advertisements on all four sides of the screen, flashing and moving, distracting you from the very content you came to the site to consume. It is also good to know that less data is being harvested from you.

The reason i choose ublock over adblock is because unlock is open source, hosted on github <here>. Adblock is closed source and for me it is important to know what code you are plugging into your browser/os

### Google Tag Assistant

Google tag assistant is great for examine all good tags/codes on the page. This helps greatly when i am checked with google analytics code/s are present on the page, saving me from scrolling through the source code. This plugin also gives feedback to if there are any problems with the tag, for example out of date or set up incorrectly.

### WebDeveloper

When I mentioned above that plugins are a way to “greatly increase browser functionality “ this one sums that sentence up, this plugin allows you to with two clicks, disable petty much anything in the browser, js, plugins, notification. Manipulate cookies, manipulate forms, and finally valet html, css at a click of a button. I mostly use this plugin for disabling JS which without is a crawl through the settings, but i am slowly implementing these other features into my workflow.

> TIP. if you are wanting to know the server rendered markup before the JS gets to it and manipulates it, disable Javascript is only two clicks away!

### ModHeader

This is one of the lesser used plugins but i am always very grateful when i need to use it. ModHeader allows you to add additional headers onto requests on a per site basis.
This is useful for when you are working on a geo targeted site locally, and need to replicate the headers that are sent along from cloud front on production. Set up once per domain and these headers will be sent overtime you request the page. A typical example of how I could use this is for setting the following header

```
CloudFront-Viewer-Country ES
```
My local codebase will now think that i am viewing from Spain

### Meta SEO Inspector

In second place to my must have Chrome plugin is Meta SEO Inspector. This handy little plugin displays all the meta tags in the response source code. It then evaluates its contents and gives suggested feedback. This again saves you from crawling through the source code to see what meta tags values are. SEO is important and this tools makes developing with geo much easier. Whats better a full list of all meta tags are only a click away!

### Vue Dev Tools

Vue is my front-end framework of choice, and this plugin gives you a very details view of the current state of your application. Many people have gone into much better depth than me, and i encourage you to learn move by going to <link>, but for a quick summary vie-dev-tools gives you the current state of all of your apps, your components within each app, the cue store, alongside logging changes and mutations which happen thought the life cycle. Events are also logged unreal time. For a VueJS developer this is a must have.

### Cookie Inspector

Cookie inspector does exactly what is sounds like, it inspects cookies. This plugin gives you another tab in your chrome dev tools and from here you can see the cookies that your website has saved to your browser allowing you to easily debug their value. Cookies can also be cleared and updated from here also.

> TIP. To clear laravel session for the currently logged in user simply delete the laravel_session cookie, saves you having to clear your cache.

### CSS Peeper

CSS peeper is a tool which is nice to have but as a backend developer I only really use to fulfill my curiosity. Essentially this will give you a clicker in which to select elements within your webpage, upon selecting these elements all the styles assigned to that element will be displayed to you. Saving you from opening dev-tools, finding the element in the markup, and scrolling down the hierarchy of styles to see which styles are appended. Only the styles rules which a most dominant are displayed, so you are saved from scrolling through a list of strike through styles to find the style you are looking for.

> TIP. All the colours used for the element are displayed in a list below, very helpful for making sure the correct colours are used.

### Clockwork

Clockwork is a Larval request degugger. A composer install is required but once that is done simply install this plugin and away you go. This appends an additional tab to your chrome dev tools and displays more helpful information about a larval request such as RequestHeaders, Get Parameters, Cookies returned
Information about the larval request cycle are also displayed such as how long each part took. Database queries are also logged and timed so this can allow you to fine tune your application and make some key optimisations. A must have for any larval developer.

> TIP. I use this heavily for counting the number of db queries my application is making, allowing me to cache whenever possible to increase request time.
