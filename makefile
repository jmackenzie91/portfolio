up:
	hugo serve -t casper -D
build:
	find ./static/images -name "*.jpg" -exec jpegoptim {} \;
	find ./static/images -name "*.jpeg" -exec jpegoptim {} \;
	find ./static/images -name "*.png" -exec optipng {} \;
	hugo -t casper